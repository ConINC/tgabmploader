#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <string>
#include <sstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

class TGA_Image
{
public:
    int w;
    int h;
    int byteperpixel;
    std::string filename;
    std::vector<unsigned char> data;
};

class BMP_Image
{
public:
    int w;
    int h;
    int byteperpixel;
    std::string filename;
    std::vector<unsigned char> data;
};


class ImageLoader
{
    public:
        ImageLoader();
        virtual ~ImageLoader();
        static BMP_Image load_bmp(std::string filename);
        static TGA_Image load_tga(std::string filename);
    protected:

    private:
};




#endif // IMAGELOADER_H
