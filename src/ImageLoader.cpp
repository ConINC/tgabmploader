#include "ImageLoader.h"

ImageLoader::ImageLoader()
{
    //ctor
}

ImageLoader::~ImageLoader()
{
    //dtor
}

BMP_Image ImageLoader::load_bmp(std::string filename)
{
    std::cout << "Loading BMP FILE" << std::endl;
    BMP_Image m;

    FILE * file;
    unsigned char *buffer;
    int w, h;
    int sz;
    //Load the bitmap
    file = fopen(filename.c_str(),"rb");
    if(file == nullptr)
    {
        std::cout << "Failed" << std::endl;
        return m;
    }
    fseek(file, 0L, SEEK_END);
    sz = ftell(file);
    //std::cout << "Size: " << sz << std::endl;

    fseek(file,18, SEEK_SET);
    fread(&w,1,4,file);
    fread(&h,1,4,file);
    fseek(file,54, SEEK_SET);

    buffer = (unsigned char *)malloc(3* w * h);
    fread(buffer, 1, w * h * 3, file);
    fclose(file);

    m.w = w;
    //std::cout << w << std::endl;
    m.h = h;
    //std::cout << h << std::endl;
    m.filename = filename;
    m.byteperpixel = 3;

    for(int i = 0; i < w*h*3; i++)
    {
        m.data.push_back(buffer[i]);
        //std::cout << +buffer[i] << std::endl;
    }

    free(buffer);

    return m;
}

TGA_Image ImageLoader::load_tga(std::string filename)
{
    std::cout << "Loading TGA FILE" << std::endl;
    TGA_Image m;

    FILE * file;
    unsigned char *buffer;
    short w, h;
    int sz;
    //Load the tga
    file = fopen(filename.c_str(),"rb");
    if(file == nullptr)
    {
        std::cout << "Failed" << std::endl;
        return m;
    }
    fseek(file, 0L, SEEK_END);
    sz = ftell(file);
    //std::cout << "Size: " << sz << std::endl;

    fseek(file,12, SEEK_SET);
    fread(&w,1,2,file);
    fread(&h,1,2,file);
    fseek(file,18, SEEK_SET);

    buffer = (unsigned char *)malloc(4* (int)w * (int)h);
    fread(buffer, 1, (int)w * (int)h * 4, file);
    fclose(file);

    m.w = (int)w;
    //std::cout << w << std::endl;
    m.h = (int)h;
    //std::cout << h << std::endl;
    m.filename = filename;
    m.byteperpixel = 4;

    for(int i = 0; i < w*h*4; i++)
    {
        m.data.push_back(buffer[i]);
        //std::cout << +buffer[i] << std::endl;
    }
    free(buffer);

    return m;
}
